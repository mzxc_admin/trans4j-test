package com.gomyck;

import com.gomyck.trans4j.DemoClass;
import com.gomyck.trans4j.Trans4JConfiguration;
import com.gomyck.trans4j.handler.dictionary.DicConverterHandler;
import com.gomyck.util.log.LoggerAutoConfig;
import com.gomyck.util.spring.mvc.version.ApiVersionAutoConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.charset.StandardCharsets;

@SpringBootTest(classes = {Trans4JConfiguration.class})
@TestPropertySource(locations = "classpath:application.yml")
@Import({LoggerAutoConfig.class, DemoClass.class, ApiVersionAutoConfig.class})
@EnableAutoConfiguration
@AutoConfigureMockMvc
public class Trans4JTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    DemoClass demoClass;

    @Autowired
    DicConverterHandler dicConverterHandler;

    @Test
    public void test() {
        try {
            ResultActions perform = mockMvc.perform(MockMvcRequestBuilders
                    .get("/test")
            );
            System.out.println(perform.andReturn()
                    .getResponse()
                    .getContentAsString(StandardCharsets.UTF_8));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
