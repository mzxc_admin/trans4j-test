package com.gomyck.trans4j;

import com.gomyck.trans4j.converter.annotation.TransEnhance;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@RestController
public class DemoClass {

    @TransEnhance
    @GetMapping("/test")
    public List<Map<String, Object>> test() {
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> result = new HashMap<>();
        result.put("plateType", "1");
        result.put("xxx", "11");
        list.add(result);
        Map<String, Object> result1 = new HashMap<>();
        result1.put("plateType", "2");
        result1.put("xxx", "22");
        list.add(result1);
        Map<String, Object> result2 = new HashMap<>();
        result2.put("plateType", "3");
        result2.put("xxx", "33");
        list.add(result2);
        Map<String, Object> result3 = new HashMap<>();
        result3.put("plateType", "4");
        result3.put("xxx", "44");

        Map<String, String> innerResult3 = new HashMap<>();
        innerResult3.put("plateType", "5");
        innerResult3.put("xxx", "55");

        result3.put("inner", innerResult3);
        DemoEntity demoEntity = new DemoEntity();
        demoEntity.setPlateType("5");

        DemoEntity demoEntity1 = new DemoEntity();
        demoEntity1.setPlateType("1");
        DemoEntity demoEntity2 = new DemoEntity();
        demoEntity2.setPlateType("2");
        DemoEntity demoEntity3 = new DemoEntity();
        demoEntity3.setPlateType("3");
        demoEntity.getAXX()[0] = demoEntity1;
        demoEntity.getAXX()[1] = demoEntity2;
        demoEntity.getAXX()[2] = demoEntity3;

        demoEntity.setBoolTest(true);
        demoEntity.setDangmark(DemoEnum.XXX);
        demoEntity.setAlarmAction("4705");
        result3.put("AAAAA", demoEntity);

        list.add(result3);
        return list;
    }


}
